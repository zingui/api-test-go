package main

import (
	"api_poc/api"
	"github.com/valyala/fasthttp"
	"log"
)

func main() {
	//runtime.GOMAXPROCS(1)
	log.Fatal(fasthttp.ListenAndServe(":6060", api.Router().Handler))
}
