package api


import (
	"github.com/buaazp/fasthttprouter"
	"api_poc/api/resources/hello"
)

func Router() *fasthttprouter.Router{

	router := fasthttprouter.New()

	router.GET("/hello", hello.Hello)
	router.POST("/hello", hello.CreateHello)

	return router
}
