package schema

import (
	"github.com/valyala/fasthttp"
	"errors"
	"api_poc/common/models"
)

var(
	ErrMissingNameField = errors.New("Missing 'name' parameter.")
)

type HelloRequest struct {
	FirstName *string `json:"firstName"`
	LastName *string `json:"lastName"`
}

type HelloResponse struct{
	Items []PersonResponse
}

type PersonResponse struct{
	Id int64 `json:"id"`
	FirstName *string `json:"firstName"`
	LastName *string `json:"lastName"`
}

func NewPersonResponse(p models.Person) *PersonResponse{
	return &PersonResponse{
		Id: p.Id,
		FirstName: &p.FirstName,
		LastName: &p.LastName,
	}
}

func (req HelloRequest) Validate(ctx *fasthttp.RequestCtx) error{

	if req.FirstName == nil {
		return ErrMissingNameField
	}

	return nil
}