package hello

import (
	"github.com/valyala/fasthttp"
	"encoding/json"
	"api_poc/api/resources/hello/schema"
	"api_poc/common"
	"api_poc/common/schema"
	"api_poc/common/models"
)

func Hello(ctx *fasthttp.RequestCtx) {

	var responseMessage schema.HelloResponse

	allPersons := models.GetAllPerson()

	for _, element := range allPersons  {
		responseMessage.Items = append(responseMessage.Items, *schema.NewPersonResponse(element))
	}

	common.RenderJSON(ctx, fasthttp.StatusOK, responseMessage)
}


func CreateHello(ctx *fasthttp.RequestCtx) {

	var parsed schema.HelloRequest

	body := ctx.Request.Body()
	err := json.Unmarshal(body, &parsed)

	err = parsed.Validate(ctx)

	if err != nil {
		errorResponse := ErrorSchema.New("parser_error", err.Error())

		common.RenderJSON(ctx, fasthttp.StatusInternalServerError, errorResponse)
		return
	}

	person := &models.Person{
		FirstName: *parsed.FirstName,
		LastName: *parsed.LastName,
	}

	person.Create()

	response := schema.NewPersonResponse(*person)

	common.RenderJSON(ctx, fasthttp.StatusOK, response)
}

