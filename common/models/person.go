package models

import (
	"log"
	"api_poc/common/db"
)

type Person struct {
	Id        int64  `db:"id"`
	FirstName string `db:"firstName"`
	LastName  string `db:"lastName"`
}

func (p *Person) Create() {
	db, _ := dbprovider.NewDatabaseClient()
	defer dbprovider.KillDatabaseClient()

	stmt, err := db.Prepare("INSERT INTO person (firstName, lastName) VALUES (?, ?)")

	if err != nil {
		log.Fatal(err.Error())
	}

	result, err := stmt.Exec(p.FirstName, p.LastName)

	if err != nil {
		log.Fatal(err.Error())
	}

	p.Id, _ = result.LastInsertId()
}

func GetAllPerson() []Person {
	db, err := dbprovider.NewDatabaseClient()
	var allPerson []Person

	if err != nil {
		log.Fatal(err.Error())
		return nil
	}

	stmt, _ := db.Prepare("SELECT id, firstName, lastName FROM person LIMIT 150")

	rows, _ := stmt.Query()

	for rows.Next() {
		var person Person

		err = rows.Scan(&person.Id, &person.FirstName, &person.LastName)
		if err != nil {
			log.Fatal(err.Error())
			continue
		}

		allPerson = append(allPerson, person)
	}

	return allPerson

}
