package common

import (
	"github.com/valyala/fasthttp"
	"encoding/json"
	"log"
	"api_poc/common/schema"
)

const (
	// ContentBinary header value for binary data.
	ContentBinary = "application/octet-stream"
	// ContentJSON header value for JSON data.
	ContentJSON = "application/json"
	// ContentJSONP header value for JSONP data.
	ContentJSONP = "application/javascript"
	// ContentLength header constant.
	ContentLength = "Content-Length"
	// ContentText header value for Text data.
	ContentText = "text/plain"
	// ContentType header constant.
	ContentType = "Content-Type"
	// ContentXML header value for XML data.
	ContentXML = "text/xml"
)

func RenderJSON(ctx *fasthttp.RequestCtx, httpCode int, v interface{}) {
	ctx.SetContentType(ContentJSON)
	ctx.SetStatusCode(httpCode)

	serialized, err := json.Marshal(v)

	if err != nil {
		log.Fatal(err.Error())
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)

		serializedBytes, _ := json.Marshal(ErrorSchema.New("internal_error", err.Error()))

		ctx.SetBody(serializedBytes)
		return
	}

	ctx.SetBody(serialized)
}
