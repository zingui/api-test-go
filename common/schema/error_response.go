package ErrorSchema

type ErrorResponse struct{
	Identifier string `json:"identifier"`
	Message string `json:"message"`
}


func New(identifier string, message string) *ErrorResponse{
	return &ErrorResponse{
		Identifier: identifier,
		Message: message,
	}
}