package common

import "github.com/valyala/fasthttp"

type InputValidation interface {
	Validate(ctx *fasthttp.Request) error
}