package dbprovider

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

type DatabaseManager struct {
	Database sql.DB
}

var instantiated *sql.DB = nil

func NewDatabaseClient() (*sql.DB, error) {
	var err error
	if instantiated == nil {
		instantiated, err = sql.Open("mysql", "root:@(localhost:3307)/person")
	}
	return instantiated, err
}

func KillDatabaseClient() {
	if instantiated != nil {
		instantiated.Close()
		instantiated = nil
	}
}
